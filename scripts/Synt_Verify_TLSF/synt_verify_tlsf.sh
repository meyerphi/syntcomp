#!/bin/bash

# Written by Jens Kreber
# Version TLSF-1.2.1

dir=$(dirname $0)

function verify () {


# tuning
ALLOW_NO_TAG=y   # will not exit if no syntcomp tag is found
#


NO_OUTPUT=-401
SYNTAX_ERROR=-403
NOT_AIGER=-402
UNREALIZABLE=15
UNREALIZABLE_NOT_RECOGNIZED=-500
REALIZABLE_NOT_RECOGNIZED=-501
MODEL_CHECKING_PASSED=1
MODEL_CHECKING_FAILED=-1
MODEL_CHECK_TIMEOUT=-24
REALIZABLE=16
MAYBE_REAL=18
MAYBE_UNREAL=17
SEEMS_LIKE_PASSED=19
VERIFIER_ERROR=-25

modelchecking_time=14400 # for 4 cores = 3600*4
origf="$1"
syntf="$2"
do_synt="$3"

if [ ! "$#" -gt 0 ]; then
    echo "Usage: $0 <original TLSF file> <output of solver> [<arbitrary string to enable synthesis mode>]"
    exit $VERIFIER_ERROR
fi

echo -n "SyntVerify (TLSF) - Running in "
if [ -n "$do_synt" ]; then
    echo "synthesis mode."
else
    echo "realizability mode."
fi

echo "Instance file: $origf"
echo -e "Solver output file: ${syntf}\n"


if [ ! -f "$syntf" ]; then
    echo -e -n "No output file found!\n\n$NO_OUTPUT"
    exit $NO_OUTPUT
fi

if [ -n "$VERIFIER_EXECUTABLES" ]; then
    modelchecker="$VERIFIER_EXECUTABLES/v3"
    aigtools="$VERIFIER_EXECUTABLES/"
else
    modelchecker="$dir/v3"
    aigtools="$dir/"
fi

syntline=$(grep -n "^[[:space:]]*//[[:space:]]*#!SYNTCOMP" "$origf" | head -n 1 | cut -d ':' -f 1)
if [ -z "$syntline" ]; then
    if [ -z $ALLOW_NO_TAG ]; then
	echo -e -n "Error: The given input file has no SYNTCOMP tag!\n\n$VERIFIER_ERROR"
	exit $VERIFIER_ERROR
    else
        echo "Warning: No SYNTCOMP tag found in the file! Will assume status \"unknown\" for further processing."
	status="unknown"
    fi
else
    synttag=$(tail -n +$syntline "$origf")
    status=$(grep "STATUS[[:space:]]*:" <<< "$synttag" | cut -d ':' -f 2 | sed "s/ //g")
fi

# Check realizability
if (grep -i -q "^UNREALIZABLE" "$syntf"); then
    case "$status" in
	"realizable")
	    echo -e -n "Realizable not recognized\n\n$REALIZABLE_NOT_RECOGNIZED"
	    exit $REALIZABLE_NOT_RECOGNIZED
	    ;;
	"unrealizable")
	    echo -e -n "Unrealizable correct\n\n$UNREALIZABLE"
	    exit $UNREALIZABLE
	    ;;
	"unknown")
	    echo -e -n "Status is unknown, the output was UNREALIZABLE\n\n$MAYBE_UNREAL"
	    exit $MAYBE_UNREAL
	    ;;
	*)
	    echo -e -n "Error: Incorrect status value.\n\n$VERIFIER_ERROR"
	    exit $VERIFIER_ERROR
    esac
elif (grep -i -q "^REALIZABLE" "$syntf"); then
    case "$status" in
	"realizable")
	    if [ -z "$do_synt" ]; then
		echo -e -n "Realizable correct\n\n$REALIZABLE"
		exit $REALIZABLE
	    fi
	    ;;
	"unrealizable")
	    echo -e -n "Unrealizable not recognized\n\n$UNREALIZABLE_NOT_RECOGNIZED"
	    exit $UNREALIZABLE_NOT_RECOGNIZED
	    ;;
	"unknown")
	    if [ -z "$do_synt" ]; then
		echo -e -n "Status is unknown, the output was REALIZABLE\n\n$MAYBE_REAL"
		exit $MAYBE_REAL
	    fi
	    ;;
	*)
	    echo -e -n "Error: Incorrect status value.\n\n$VERIFIER_ERROR"
	    exit $VERIFIER_ERROR
    esac
fi

# allow no output in synthesis mode for legacy reasons
if [ -z "$do_synt" ]; then
    echo -e -n "Found nor REALIZABLE nor UNREALIZABLE\n\n$NO_OUTPUT"
    exit $NO_OUTPUT
fi

## Check synt
# Solver (must) output "REALIZABLE" immediately before output aiger!
realline=$(grep -i -n -m 1 "^REALIZABLE" "$syntf" | head -n 1 | cut -d ':' -f 1)
aagline=$(grep -n -m 1 "^aag " "$syntf" | head -n 1 | cut -d ':' -f 1) # legacy
if [ -z "$realline" ]; then
    if [ -z "$aagline" ]; then
	echo -e -n "Could find nor REALIZABLE nor aag.\n\n$NO_OUTPUT"
	exit $NO_OUTPUT
    else
	echo "Warning: Found no REALIZABLE but aag in line $aagline. This is deprecated."
    fi
else
    if [ -z "$aagline" ]; then
	echo -e -n "Found REALIZABLE in line $realline but no aag header. This is strange.\n\n$NOT_AIGER"
	exit $NOT_AIGER
    else # realline and aagline exist
	if ! [ "$aagline" -eq $(($realline + 1)) ]; then
	    if [ "$aagline" -lt "$realline" ]; then
		echo "Warning: REALIZABLE found in line $realline, but aag was found already in line $aagline."
	    else
		echo -e -n "REALIZABLE found in line $realline, but first aag header in line $aagline. This should really not happen.\n\n$NOT_AIGER"
		exit $NOT_AIGER
	    fi
	fi
    fi
fi


tail -n +"$aagline" "$syntf" > "${syntf}.aag"
# Generate monitor
syfco --format smv "$origf" -m fully | "$aigtools/smvtoaig" > "monitor.aag" 2> /dev/null
"$aigtools/combine-aiger" monitor.aag "${syntf}.aag" > "${syntf}_combined.aag"
if ! (head -n 1 "${syntf}_combined.aag" | grep -q '^aag'); then
    echo -e -n "Error during monitor combination!\n\n$VERIFIER_ERROR"
    exit $VERIFIER_ERROR
fi
"$aigtools/aigtoaig" "${syntf}_combined.aag" "${syntf}_combined.aig"

## Model checking
ulimit -t "$modelchecking_time"
check_res=$("$modelchecker" "${syntf}_combined.aig")
res_val=$?
check_res_first=$(head -n 1 <<< "$check_res")
if [[ "$check_res_first" =~ ^0$ ]];  then
    echo "Model checking succesfull!"
elif [[ $res_val == 137 || $res_val == 152 || $res_val == 143 ]]; then  # Killed or stopped
    rm -f "${syntf}.aag" "monitor.aag" "${syntf}_combined.aag" "${syntf}_combined.aig"
    echo -e -n "Model checking timed out or process was killed somehow!\n\n$MODEL_CHECK_TIMEOUT"
    exit $MODEL_CHECK_TIMEOUT
else
    # do not clean up!
    echo -e -n "Model-checking the resulting circuit failed! v3 output: \n$check_res\n\n$MODEL_CHECKING_FAILED"
    exit $MODEL_CHECKING_FAILED
fi


## (Reference) circuit size
size_latches=$(head -n 1 "${syntf}.aag" | cut -d ' ' -f 4)
size_ands=$(head -n 1 "${syntf}.aag" | cut -d ' ' -f 6)

echo -e "\nSynthesis latches: $size_latches"
echo "Synthesis and gates: $size_ands"

size_synt=$(($size_latches + $size_ands))
size_ref=$(grep "REF_SIZE[[:space:]]*:" <<< "$synttag" | cut -d ':' -f 2 | sed "s/ //g")


if [ -n "$size_ref" ]; then
    diff_ref=$((size_synt - size_ref))
    if [ ! "$size_ref" -eq 0 ]; then
	outbyref=$(echo -e "scale=5\n$size_synt / $size_ref\n" | bc -l | sed 's/^\./0./')
    fi
    echo -e -n "Reference circuit size: $size_ref\nDifference to reference: $diff_ref\nOutput by reference: $outbyref\n"
fi

echo -e -n "\n"

# Clean up
rm -f "${syntf}.aag" "monitor.aag" "${syntf}_combined.aag"


if [ "$status" == "unknown" ]; then
    rm -f "${syntf}_combined.aig"
    echo -e -n "This is nice. Status was unknown before but this tool passed all checks with a valid result. Seems like it is realizable.\n\n$SEEMS_LIKE_PASSED"
    exit $SEEMS_LIKE_PASSED
else
    rm -f "${syntf}_combined.aig"
    echo -e -n "All checks passed!\n\n$MODEL_CHECKING_PASSED"
    exit $MODEL_CHECKING_PASSED
fi

}

verify "$@" 2>&1
