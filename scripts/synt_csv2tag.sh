#!/bin/bash

_tle="\"time limit exceeded\""
_real="\"Realizable\""
_unreal="\"Unrealizable\""
_sunrealbn="\"Realizable not recognized\""
_srealbn="\"Unrealizable not recognized\""
_mayreal="\"Maybe realizable\""
_mayunreal="\"Maybe unrealizable\""
_finished="\"finished\""
_crashed="\"solver crash\""
_sigsegv="\"solver SIGSEGV\""
_sigabort="\" hohoho \""
_passed="\"Model checking passed\""
_slpassed="\"Seems like passed\""

if [ ! "$#" -eq 8 ]; then
    echo "Usage: $0 <aiger file> <csv file> <solver count> <name of this run> real/synt seq/par useResult/useMajority/useSingle [STATUS[,SOLVED_BY[,SOLVED_IN,[REF_SIZE]]]]"
    exit 1
fi


# Argument processing
file="$1"
instance=$(basename "$1")
csv="$2" 
solver_count="$3"
runname="$4"
realsynt="$5"
seqpar="$6"
statusMode="$7"
params="$8"


## Determine tags to compute
if [[ "$params" =~ "STATUS" ]]; then
    do_status=y
fi
if [[ "$params" =~ "SOLVED_BY" ]]; then
    do_solvedby=y
fi
if [[ "$params" =~ "SOLVED_IN" ]]; then
    do_solvedin=y
fi
if [[ "$params" =~ "REF_SIZE" ]]; then
    do_refsize=y
fi


if [ "$seqpar" == "seq" ]; then
    seqrun=y
elif [ "$seqpar" == "par" ]; then
    parrun=y
else
    echo "You must specify if you want to analyse either a sequential or a parallel track!"
    exit 1
fi

 if [ "$realsynt" == "real" ]; then
    realrun=y
elif [ "$realsynt" == "synt" ]; then
    syntrun=y
else
    echo "You must specify if you want to analyse either a realizability or a synthesis track!"
    exit 1
fi

if [[ -z "$syntrun" && -n "$do_refsize" ]]; then
    echo "Can only determine reference size for synthesis runs!"
    exit 1
fi

if [ "$statusMode" == "useResult" ]; then # trust the verifier
    useResult=y
elif [ "$statusMode" == "useMajority" ]; then # trust the (majority) of solvers
    useMajority=y
elif [ "$statusMode" == "useSingle" ]; then # trust all solvers (one result is sufficient)
    useMajority=y
    useSingle=y
fi


# get the required column indizes
header=$(head -n 1 "$csv" | tr , '\n') # substitute "," by "\n" for use with grep -n
csolver=$(grep -n "\"Solver\""  <<< "$header" | cut -d ':' -f 1) # find column index of solver
csolverconfig=$(grep -n "\"Solver Configuration\""  <<< "$header" | cut -d ':' -f 1) # ... and so on ...
cinstance=$(grep -n "\"Instance\""  <<< "$header" | cut -d ':' -f 1)
ctime=$(grep -n "\"Time\""  <<< "$header" | cut -d ':' -f 1)
cwtime=$(grep -n "\"Wall Time\""  <<< "$header" | cut -d ':' -f 1)
cstatus=$(grep -n "\"Status\""  <<< "$header" | cut -d ':' -f 1)
cresult=$(grep -n "\"Result Code\""  <<< "$header" | cut -d ':' -f 1)
ccost=$(grep -n "\"Cost\""  <<< "$header" | cut -d ':' -f 1)

if [ -z "$csolver" ]; then
    echo "Could not get solver column. Export it please!"
    exit 1
fi
if [ -z "$cinstance" ]; then
    echo "Could not get instance column. Export it please!"
    exit 1
fi
if [ -z "$cresult" ]; then
    echo "Could not get result column. Export it please!"
    exit 1
fi
if [[ -n "$seqrun" && -z "$ctime" ]]; then
    echo "Could not get time column. Export it please!"
    exit 1
fi
if [[ -n "$parrun" && -z "$cwtime" ]]; then
    echo "Could not get wall time column. Export it please!"
    exit 1
fi
if [[ -n "$do_refsize" && -z "$ccost" ]]; then
    echo "Could not get cost column. Export it please!"
    exit 1
fi

# filter only the current instance
entries=$(grep "\"$instance\"" "$csv")

declare -A said
declare -A timeused
declare -A circsize

function mintime () {
    if [ -z "$parrun" ]; then
	time=${current[$((ctime-1))]}
    else
	time=${current[$((cwtime-1))]}
    fi
    time=$(sed s/\"//g <<< "$time")
    if [ -z "$time" ]; then
	echo "Error: Could not get time! Are you sure you exported it?"
	exit 1
    fi

    # echo "time is $time"
    if [ -z "${timeused[$curr_solver]}" ]; then
	timeused[$curr_solver]=$time
    else
	#   echo "VS: $time < ${timeused[$curr_solver]}"
	if [ $(echo "$time < ${timeused[$curr_solver]}" | bc -l) -eq 1 ]; then  # <3 Linux & bash
	    timeused[$curr_solver]=$time
	else
	    :
       fi
    fi
}

function minsize () {
    csize=$(sed s/\"//g <<< "${current[$((ccost-1))]}")

     if [ -z "$csize" ]; then
	echo "Error: Could not get the cost! Are you sure you exported it?"
	exit 1
    fi  
    if [ -z "${circsize[$curr_solver]}" ]; then
	circsize[$curr_solver]=$csize
    else
	if [ $(echo "$csize < ${circsize[$curr_solver]}" | bc -l) -eq 1 ]; then
	    circsize[$curr_solver]=$csize
	else
	    :
       fi
    fi
}


echo -e "$instance:"
solvedby=0

# !!! SHOPT
shopt -s nocasematch

i=0
# step through all entries
while IFS=',' read -a current; do # input at done
    ((i++))
    curr_solver="${current[$((csolver-1))]}"
    curr_solver=$(sed s/[^[:alnum:]]//g <<< "$curr_solver")
    curr_solver="${i}_$curr_solver"           # ! :/ (different configurations)
    if [[ ! "$solvers" =~ "$curr_solver" ]]; then
	solvers="$solvers $curr_solver"
    else # had this solver (is this even possible..?) TODO
	:
    fi
    currres=${current[$((cresult-1))]} # current result
    if [[ ( -z "$curr_solver") || ( -z "$currres") ]]; then
	echo "I couldn't get the solver and and/or the result. Maybe this instance is not in your csv table?"
	if [ -n "$do_refsize" ]; then
	    dir=`dirname $0`
	    echo "No reference size found in csv. Reference size will be -1."
    	    status="unknown"
	    circsize_max="-1"
	fi
    fi
    echo -n " $curr_solver"
###
# This is thought of being able to distinguish several results from the same solver (multiple runs, configurations).
# Not really used atm.
##
    if [[ "${said[$curr_solver]}" == "real" ]]; then
	if [[ "$currres" =~ ^("$_real"|"$_srealbn"|"$_mayreal"|"$_passed"|"$_slpassed")$ ]]; then
	    resassign="real"
	elif [[ "$currres" =~ ^("$_unreal"|"$_sunrealbn"|"$_mayunreal")$ ]]; then
	    resassign="Error: Solver $curr_solver once said real but said unreal now. Quitting."
	else
	    resassign="wasreal"
	fi
    elif [[ "${said[$curr_solver]}" == "unreal" ]]; then
	if [[ "$currres" =~ ^("$_real"|"$_srealbn"|"$_mayreal")$ ]]; then
	    resassign="Error: Solver $curr_solver once said unreal but said real now. Quitting."
	    exit 1
	elif [[ "$currres" =~ ^("$_unreal"|"$_sunrealbn"|"$_mayunreal")$ ]]; then	    
	    resassign="unreal"
	else
	    resassign="wasunreal"
	fi
    else # mostly the case now
	if [[ "$currres" =~ ^("$_real"|"$_srealbn"|"$_mayreal"|"$_passed"|"$_slpassed")$ ]]; then
	    said[$curr_solver]="real"
	    resassign="nowreal"
	elif [[ "$currres" =~ ^("$_unreal"|"$_sunrealbn"|"$_mayunreal")$ ]]; then
	    said[$curr_solver]="unreal"
	    resassign="nowunreal"
	else
	    said[$curr_solver]="unknown"
	    resassign="unknown"
	fi
	echo " said $(sed s/^now// <<< $resassign)"
    fi 
###

    #echo "currres: $currres, $resassign, ${said[*]}"
    if [[ "$resassign" =~ ^Error: ]]; then
	echo "$resassign"
	exit 1
    fi
    if [[ (-n "$useResult") && ("$currres" =~ ^($_real|$_unreal|$_passed|$_slpassed)$) ]]; then
	mintime
	minsize
	status="${said[$curr_solver]}izable"
	if [[ "$resassign" =~ ^now ]]; then
	    ((solvedby++))
	fi
    elif [[ (-n "$useMajority") && ("$resassign" =~ ^(now)?(un)?real$) ]] ; then
	    mintime
	    minsize
	if [[ "$resassign" =~ ^now ]]; then
	    ((solvedby++))
	fi	    
    fi
done <<< "$entries" # epic bash magic

## !!! SHOPT
shopt -u nocasematch


solvers_found=0
votereal=0
voteunreal=0
voteunknown=0
# step through all solvers
for sol in $solvers; do
    if [ -n "$useMajority" ]; then
	if [ "${said[$sol]}" == "real" ]; then
	    ((votereal++))
	elif [ "${said[$sol]}" == "unreal" ]; then
	    ((voteunreal++))
	elif [ "${said[$sol]}" == "unknown" ]; then
	    ((voteunknown++))
	else
	    echo "Sanity error voting. Output: ${said[$sol]}"
	    exit 1
	fi
    fi
    if [ -n "$do_solvedin" ]; then
	if [ -z "${timeused[$sol]}" ]; then
	    : # keep solvedin_max
	else
	    if [ -z "$solvedin_max" ]; then
		solvedin_max=${timeused[$sol]} # update
	    else # both existent
	#	echo "VS: $solvedin_max < ${timeused[$sol]}"
		if [ $(echo "$solvedin_max < ${timeused[$sol]}" | bc -l) -eq 1 ] ; then
		  #  echo smaller
		    : # keep solvedin_max
		else
		  #  echo greater
		    solvedin_max=${timeused[$sol]} # update
		fi
	    fi
	fi
    fi
    if [ -n "$do_refsize" ]; then
	if [ -z "${circsize[$sol]}" ]; then
	    : # keep circsize_max
	else
	    if [ -z "$circsize_max" ]; then
	        circsize_max=${circsize[$sol]} # update
	    else # both existent
		if [ $(echo "$circsize_max < ${circsize[$sol]}" | bc -l) -eq 1 ] ; then
		    : # keep solvedin_max
		else
		    circsize_max=${circsize[$sol]} # update
		fi
	    fi
	fi
    fi
    ((solvers_found++))
done

#if [ ! "$solvers_found" -eq "$solver_count" ]; then
#    echo "Error: For instance $instance, there were only $solvers_found solver results but $solver_count expected!"
#    exit 1
#fi
if [ -n "$useMajority" ]; then
    if [ ! "$solvers_found" -eq $((votereal + voteunreal + voteunknown)) ]; then
    echo "Sanity error, vote checking. Votes were: $votereal real, $voteunreal unreal, $voteunknown unknown. Found 3 solvers."
    exit 1
    fi
    if [ "$votereal" -gt "0" ]; then
	if [ "$voteunreal" -eq "0" ]; then
	    if [ "$votereal" -gt "$voteunknown" ]; then
		status="realizable"
	    else
		if [ -n "$useSingle" ]; then
		    status="realizable"
		else
		    echo "This may be real, $votereal solvers voted for real but the number of unknowns is too high: $voteunknown. Use mode \"useSingle\" if you want to accept a single vote."
		    exit 1
		fi
	    fi
	else
	    echo "There were solvers that said real and some that said unreal. I will not resolve this situation. Good luck."
	    exit 1
	fi
    elif [ "$voteunreal" -gt "0" ]; then
	if [ "$voteunreal" -gt "$voteunknown" ]; then
	    status="unrealizable"
	else
	    if [ -n "$useSingle" ]; then
		status="unrealizable"
	    else
		echo "This may be unreal, $voteunreal solvers voted for unreal but the number of unknowns is too high: $voteunknown. Use mode \"useSingle\" if you want to accept a single vote."
		exit 1
	    fi
	fi
    else
	: # status unknown
    fi
fi


dir=`dirname $0`
# compute final result
if [ -n "$do_status" ]; then
    if [ -z "$status" ]; then
	status="unknown"
    fi
    echo "The status of this instance is now \"$status\""
    # added this distinction: do not overwrite exisiting status with "unknown"    
    #if [ "$status" != "unknown" ]; then
    #"$dir/synt_tag.sh" r "//STATUS" "$file"
    "$dir/synt_tag.sh" a "//STATUS=$status" "$file"
    #else
    #echo "Will not replace existing status information in file"
    #fi
fi
if [ -n "$do_solvedby" ]; then
    echo "Solved by $solvedby out of $solver_count"
    "$dir/synt_tag.sh" r "SOLVED_BY=[[:digit:]]\+/[[:digit:]]\+ [[]$runname[]]" "$file"
    "$dir/synt_tag.sh" u "SOLVED_BY=$solvedby/$solver_count [$runname]" "$file" 
fi
if [ -n "$do_solvedin" ]; then
    "$dir/synt_tag.sh" r "SOLVED_IN=[[:digit:]]\+\.[[:digit:]]\+ [[]$runname[]]" "$file"
    if [ -z "$solvedin_max" ]; then
	echo "Was not solved, time value will be 0."
	"$dir/synt_tag.sh" u "SOLVED_IN=0.0 [$runname]" "$file"
    else
	echo "Fastest solve in $solvedin_max"
	"$dir/synt_tag.sh" u "SOLVED_IN=$solvedin_max [$runname]" "$file"
    fi
fi
if [ -n "$do_refsize" ]; then
     #"$dir/synt_tag.sh" r "//REF_SIZE" "$file"
    if [ -z "$circsize_max" ]; then
	echo "Seems like it was not solved. Reference size will be -1."
	"$dir/synt_tag.sh" a "//REF_SIZE=-1" "$file"
    else
	circsize_max=$(sed s/\\.\.*$//g <<< "$circsize_max")
	echo "Smallest circuit was $circsize_max"
	"$dir/synt_tag.sh" a "//REF_SIZE=$circsize_max" "$file"
    fi
fi
	
