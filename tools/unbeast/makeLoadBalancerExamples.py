#! /usr/bin/env python
# -*- coding: ISO-8859-15 -*-
# 
# Python script to create examples for the CAV 2010 paper
#
from __future__ import with_statement
import os
import pprint
import binascii
import md5
import lxml
import inspect
from lxml import etree
from lxml import sax
from xml import dom
from xml.dom.minidom import Node
from xml.dom.pulldom import SAX2DOM

# =====================================================================
# General Procedures & XMLLing
# =====================================================================
def make_all_pairs(elements):
    """Takes a list of element and returns the list of all pairs
    of these. So if there are 3 elements given, it returns a list of size
    9. """

    res = []
    for a in elements:
      for b in elements:
        res.append((a,b))
    return res

def stringRepeat(kette,repetitions):
    """ stringRepeat(s,n) = s+s+s+s+s+... (n times!)"""
    if repetitions==1:
        return kette
    return kette+stringRepeat(kette,repetitions-1)

def dictUnion(one,two):
    """ Creates a new dictionary (shallow copy (?)) with the entries of 
    the first and second dictionary. Entries in the second one overwrite
    those in the first one."""
    newDict = {}
    newDict.update(one)
    newDict.update(two)
    return newDict


# =====================================================================
# Properties/Assumptions to be synthesized
# =====================================================================
def WellFormednessProperty1(nofClients,specFile,propString):
    specFile.write("    <!--Basic well-formedness property 1: No grant without request-->\n")
    for i in range(0,nofClients):
        specFile.write("    <LTL><G><Or><Not><Var>grant"+str(i)+"</Var></Not><Var>request"+str(i)+"</Var></Or></G></LTL>\n")
    specFile.write("\n")
    propString.append("1")

def WellFormednessProperty2(nofClients,specFile,propString):
    specFile.write("    <!--Basic well-formedness property 2: grants are not given at the same time-->\n")
    specFile.write("    <LTL><G><And>\n")
    for i in range(0,nofClients):
        for j in range(0,nofClients):
            if i<>j:
                specFile.write("      <Or><Not><Var>grant"+str(i)+"</Var></Not><Not><Var>grant"+str(j)+"</Var></Not></Or>\n")
    specFile.write("    </And></G></LTL>\n")
    specFile.write("\n")
    propString.append("2")

def wrongLivenessProperty(nofClients,specFile,propString):
    specFile.write("    <!--Wrong liveness property: grants are given infinitely often-->\n")
    for i in range(0,nofClients):
        specFile.write("    <LTL><G><F><Var>grant"+str(i)+"</Var></F></G></LTL>\n")
    specFile.write("\n")
    propString.append("3")

def LivenessProperty(nofClients,specFile,propString):
    specFile.write("    <!--Basic liveness property: Every request is eventually handled-->\n")
    for i in range(0,nofClients):
        specFile.write("    <LTL><Or><Not><G><F><Var>request"+str(i)+"</Var></F></G></Not><G><F><Var>grant"+str(i)+"</Var></F></G></Or></LTL>\n")
    specFile.write("\n")
    propString.append("4")

def waitUntilDoneProperty(nofClients,specFile,propString):
    specFile.write("    <!-- The implementation waits until 'idle' is given before giving grants -->\n")
    specFile.write("    <LTL><G><Or><And>\n")
    for i in range(0,nofClients):
        specFile.write("      <Not><Var>grant"+str(i)+"</Var></Not>\n")
    specFile.write("    </And><Var>idle</Var></Or></G></LTL>\n")
    specFile.write("\n")
    propString.append("5")

def idlingGuarantee(nofClients,specFile,propString):
    specFile.write("    <!-- The processing unit is always eventually idle -->\n")
    specFile.write("    <LTL><G><F><Var>idle</Var></F></G></LTL>\n")
    specFile.write("\n")
    propString.append("6")

def idleSignalStaysUntilgrantGuarantee(nofClients,specFile,propString):
    specFile.write("    <!-- Idle stays true until some grant has been given -->\n")
    specFile.write("    <LTL><G><Or><Not><And>\n")
    specFile.write("      <Var>idle</Var>\n")
    for i in range(0,nofClients):
        specFile.write("      <Not><Var>grant"+str(i)+"</Var></Not>\n")
    specFile.write("    </And></Not><X><Var>idle</Var></X></Or></G></LTL>\n")
    specFile.write("\n")
    propString.append("7")

def ModifiedLivenessProperty(nofClients,specFile,propString):
    specFile.write("    <!--Modified liveness property: Every request that stays on is eventually handled-->\n")
    for i in range(0,nofClients):
        specFile.write("    <LTL><Not><F><G><And><Var>request"+str(i)+"</Var><Not><Var>grant"+str(i)+"</Var></Not></And></G></F></Not></LTL>\n")
    specFile.write("\n")
    propString.append("8")

def priorityForFirstrequest(nofClients,specFile,propString):
    specFile.write("    <!-- Priority for the first request -->\n")
    for i in range(1,nofClients):
        specFile.write("    <LTL><G><Or><Not><Var>request0</Var></Not><Not><Var>grant"+str(i)+"</Var></Not></Or></G></LTL>\n")
    specFile.write("\n")
    propString.append("9")

def request0isNiceAssumption(nofClients,specFile,propString):
    specFile.write("    <!--Assumption that request 0 is nice, i.e. request for 0 remains low for at least 1 more time units after idle is true again whenever grant0 is given -->\n")
    specFile.write("    <LTL><G><Or><Not><Var>grant0</Var></Not><X><U><And><Not><Var>request0</Var></Not><Not><Var>idle</Var></Not></And><And><Not><Var>request0</Var></Not><Var>idle</Var></And></U></X></Or></G></LTL>\n")
    specFile.write("\n")
    propString.append("10")

# def waitBeforegrant(waiting,nofClients,specFile,propString):
# Here's something to do: This guarantee doesn't work well with the last assumption: request 0 might be given again and again during this grace period, ruling out the possibility to let someone else get a grant first, thus contradicting liveness. For this paper, we shorten the example.
#    specFile.write("    <!--A little bit of waiting has to be added before each grant / small version -->\n")
#    specFile.write("    <LTL><G><Or><Not><And><Not><Var>idle</Var></Not><X><Var>idle</Var></X></And></Not> \n")
#    for j in range(0,waiting):    
#        specFile.write("      <X><And>\n")
#        for i in range(0,nofClients):
#            specFile.write("      <Not><Var>grant"+str(i)+"</Var></Not> \n")
#
#    for j in range(0,waiting):    
#        specFile.write("      </And></X>\n")
#    specFile.write("    </Or></G></LTL>\n")      
#    specFile.write("\n")
#    propString.append("11("+str(waiting)+")")

# =====================================================================
# List of cases to consider
# =====================================================================
cases = [\
         # Satisfiable
         ([],[WellFormednessProperty1,WellFormednessProperty2]),\

         # Satisfiable
         ([],[WellFormednessProperty1]),\

         # Unsat: Can only give grants if request are given
         ([],[WellFormednessProperty1,WellFormednessProperty2,wrongLivenessProperty]),\

         # OK
         ([],[WellFormednessProperty1,WellFormednessProperty2,LivenessProperty]),\

         # Unsat: "idle" might never be given.
         ([],[WellFormednessProperty1,WellFormednessProperty2,LivenessProperty,waitUntilDoneProperty]),\

         # Not satisfiable. idling and granting may interleaved
         ([idlingGuarantee],[WellFormednessProperty1,WellFormednessProperty2,LivenessProperty,waitUntilDoneProperty]),\

         # Still not satisfiable -- request 0 may always come after *after* grant 1 has been given - Expensive check!
         ([idleSignalStaysUntilgrantGuarantee,idlingGuarantee],[WellFormednessProperty1,WellFormednessProperty2,LivenessProperty,waitUntilDoneProperty]),\

         # Fixed. Uses modified liveness property
         ([idleSignalStaysUntilgrantGuarantee,idlingGuarantee],[WellFormednessProperty1,WellFormednessProperty2,ModifiedLivenessProperty,waitUntilDoneProperty]),\

         # Adding priority for the first component - Liveness for the others not possible anymore!
         ([idleSignalStaysUntilgrantGuarantee,idlingGuarantee],[WellFormednessProperty1,WellFormednessProperty2,ModifiedLivenessProperty,waitUntilDoneProperty,priorityForFirstrequest]),\

         # Adding More timing assumptions: request for 0 remains low for at least 1 more time units after idle is true again whenever grant0 is given - SAT!
         ([idleSignalStaysUntilgrantGuarantee,idlingGuarantee,request0isNiceAssumption],[WellFormednessProperty1,WellFormednessProperty2,ModifiedLivenessProperty,waitUntilDoneProperty,priorityForFirstrequest]),\

         # Flushing period: Must wait for 3 cycles until grant can be given - SAT
         #([idleSignalStaysUntilgrantGuarantee,idlingGuarantee,request0isNiceAssumption],[lambda x,y,z: waitBeforegrant(3,x,y,z),WellFormednessProperty1,WellFormednessProperty2,ModifiedLivenessProperty,waitUntilDoneProperty,priorityForFirstrequest]),\

         # Flushing period: Must wait for 7 cycles until grant can be given - SAT
         #([idleSignalStaysUntilgrantGuarantee,idlingGuarantee,request0isNiceAssumption],[lambda x,y,z: waitBeforegrant(7,x,y,z),WellFormednessProperty1,WellFormednessProperty2,ModifiedLivenessProperty,waitUntilDoneProperty,priorityForFirstrequest])\

        ]


# =====================================================================
# Benchmark settings
# =====================================================================
possibleNofClients = range(2,10)

# --> Old options
# LTLCompiler = "~/LocalLibs/spot-0.4/src/tgbatest/ltl2tgba -N "
# LTLCompiler = "~/LocalLibs/spot-0.4/src/tgbatest/ltl2tgba -N -m -p -r1 -r2 -r3 -r4 -r5 -r6 -r7 "


# --> Options used in the paper
LTLCompiler = "~/LocalLibs/ltl2ba-1.1/ltl2ba -f"
# LTLCompiler = "~/LocalLibs/spot-0.4/src/tgbatest//ltl2tgba -N -R3 -fr7 -fr4 "

    

# =====================================================================
# Write the actual source files
# =====================================================================
nofFilesSoFar = 0

with open("caseList.txt","w") as caseListFile:
    for ((assumptions,guarantees),nofClients) in [(x,y) for x in cases for y in possibleNofClients]:
        filename = "load_"+str(nofFilesSoFar)+".xml"
        with open(filename,"w") as destFile:

            specPre = []
            specPost = []

            # Write preamble
            destFile.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n")
            destFile.write("<!DOCTYPE SynthesisProblem SYSTEM \"../SynSpec.dtd\">\n")
            destFile.write("<!-- Synthesis Problem for Distributed synthesis under full information (but limited delay) -->")
            destFile.write("<SynthesisProblem>\n")
            destFile.write("  <!-- Some general properties -->\n")
            destFile.write("  <Title>A load balancing example</Title>\n")
            destFile.write("  <Description>A load balancing variant, "+str(nofClients)+" clients</Description>\n")
            destFile.write("  <PathToLTLCompiler>"+LTLCompiler+"</PathToLTLCompiler>\n\n")
            destFile.write("  <!-- Declare Global inputs -->\n")
            destFile.write("  <GlobalInputs>\n")
            destFile.write("    <Bit>idle</Bit>\n")
            for i in range(0,nofClients):
                destFile.write("    <Bit>request"+str(i)+"</Bit>\n")
            destFile.write("  </GlobalInputs>\n\n")

            destFile.write("  <!-- Declare Global outputs -->\n")
            destFile.write("  <GlobalOutputs>\n")
            for i in range(0,nofClients):
                destFile.write("    <Bit>grant"+str(i)+"</Bit>\n")
            destFile.write("  </GlobalOutputs>\n\n")

            destFile.write("  <!-- Assumptions -->\n")
            destFile.write("  <Assumptions>\n")
            for a in assumptions:
                a(nofClients,destFile,specPre)
            destFile.write("  </Assumptions>\n\n")

            destFile.write("  <!-- Guarantees -->\n")
            destFile.write("  <Specification>\n")
            for a in guarantees:
                a(nofClients,destFile,specPost)
            destFile.write("  </Specification>\n\n")

            destFile.write("</SynthesisProblem>\n")

        # Done? Register specification file
        nofFilesSoFar += 1
        destFile.close()

        specPre.sort(lambda x,y: cmp(int(x.split("(")[0]),int(y.split("(")[0])))
        specPost.sort(lambda x,y: cmp(int(x.split("(")[0]),int(y.split("(")[0])))
        caseListFile.write(filename+"\n")
        caseListFile.write(str(nofClients)+"\n")
        caseListFile.write("$"+(" \wedge ".join(specPre)))
        if (len(specPre)>0):
            caseListFile.write("\\rightarrow")
        caseListFile.write(" \wedge ".join(specPost))
        caseListFile.write("$\n")

caseListFile.close()
