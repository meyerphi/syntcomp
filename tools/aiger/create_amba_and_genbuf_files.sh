#!/bin/bash

# This script creates Verilog-checkers for the AMBA [1] and GENBUF [2]
# synthesis benchmarks. The Verilog-checkers are translations of the
# benchmark files for ANZU [3] and RATSY [4] into Verilog. The
# Verilog-checkers have as input all inputs and outputs of the system to be
# synthesized. The Verilog-checkers have only one Boolean output, which
# signals an error. This error output must never go to TRUE for a correct
# implementation of the benchmarks. (This is a simple safety property.)
# The original AMBA and GENBUF benchmarks contain also fairness (Buechi)
# assumptions and guarantees. They are translated into safety properties as
# follows.
# - We reduce all fairness assumptions into one fairness assumption X using
#   (different variants of) a counting construction.
# - We reduce all fairness guarantees into one fairness guarantee Y using
#   (different variants of) a counting construction.
# - We add one more counter Z. It is incremented whenever X visits and accepting
#   state. It is reset whenever there is some progress in the counting
#   construction of the fairness guarantees. It must never
#   reach a given value (otherwise the error-output will go to TRUE).
#   This enforces a certain ratio between fairness guarantee achievements and
#   fairness assumption achievements.
# - At the moment, we use two different variants of counting constructions:
#   (1) Using one bit per fairness property to track if it was already visited.
#       This is indicated with a 'b' in the benchmark name.
#   (2) Using a counter which stores the index of the next fairness property
#       that must be visited. This is indicated with a 'c' in the benchmark
#       name.
# - We also create benchmarks were the counter Z is reset only when Y visits an
#   accepting state. This is indicated with an 'f' in the benchmark name.
# [1] http://www.iaik.tugraz.at/content/research/design_verification/anzu/DAC07amba.pdf
# [2] http://www.iaik.tugraz.at/content/research/design_verification/anzu/cocv.pdf
# [3] http://www.iaik.tugraz.at/content/research/design_verification/anzu/
# [4] http://rat.fbk.eu/ratsy/

#----------------------------------------------------------------------------
# Creating benchmarks using ahb_spec_generator_new_1.5c_vlog.pl:
#----------------------------------------------------------------------------
# - it resets the counter (counting how often all fairness assumptions
#   have already been fulfilled) whenever there is SOME progress on the
#   fairness guarantees (and not just when all fairness guarantees have been
#   satisfied in a row)
# - it uses a counter to track fairness property achievements instead
#   of one bit per fairness property
#----------------------------------------------------------------------------
perl ./ahb_spec_generator_new_1.5c_vlog.pl  2  6 > ./amba2c6unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  2  7 > ./amba2c7.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  3  4 > ./amba3c4unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  3  5 > ./amba3c5.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  4  6 > ./amba4c6unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  4  7 > ./amba4c7.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  5  4 > ./amba5c4unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  5  5 > ./amba5c5.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  6  4 > ./amba6c4unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  6  5 > ./amba6c5.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  7  4 > ./amba7c4unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  7  5 > ./amba7c5.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  8  6 > ./amba8c6unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  8  7 > ./amba8c7.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl  9  4 > ./amba9c4unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl  9  5 > ./amba9c5.v

perl ./ahb_spec_generator_new_1.5c_vlog.pl 10  4 > ./amba10c4unreal.v
perl ./ahb_spec_generator_new_1.5c_vlog.pl 10  5 > ./amba10c5.v


#----------------------------------------------------------------------------
# Creating benchmarks using ahb_spec_generator_new_1.5b_vlog.pl:
#----------------------------------------------------------------------------
# - it resets the counter (counting how often all fairness assumptions
#   have already been fulfilled) whenever there is SOME progress on the
#   fairness guarantees (and not just when all fairness guarantees have been
#   satisfied in a row)
# - it uses one bit per fairness property to track if the fairness
#   property has already been visited (instead of a counter)
#----------------------------------------------------------------------------
perl ./ahb_spec_generator_new_1.5b_vlog.pl  2  8 > ./amba2b8unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  2  9 > ./amba2b9.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  3  4 > ./amba3b4unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  3  5 > ./amba3b5.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  4  8 > ./amba4b8unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  4  9 > ./amba4b9.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  5  4 > ./amba5b4unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  5  5 > ./amba5b5.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  6  4 > ./amba6b4unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  6  5 > ./amba6b5.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  7  4 > ./amba7b4unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  7  5 > ./amba7b5.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  8  5 > ./amba8b5unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  8  6 > ./amba8b6.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl  9  4 > ./amba9b4unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl  9  5 > ./amba9b5.v

perl ./ahb_spec_generator_new_1.5b_vlog.pl 10  4 > ./amba10b4unreal.v
perl ./ahb_spec_generator_new_1.5b_vlog.pl 10  5 > ./amba10b5.v

#----------------------------------------------------------------------------
# Creating benchmarks using ahb_spec_generator_new_1.5f_vlog.pl:
#----------------------------------------------------------------------------
# - it use a bit per fairness property to track if the fairness
#   property has already been visited (instead of a counter)
# - it resets the counter (counting how often all fairness assumptions
#   have already been fulfilled) only if all fairness guarantees have been
#   satisfied in a row (and not if there is some progress in the guarantees).
#----------------------------------------------------------------------------
perl ./ahb_spec_generator_new_1.5f_vlog.pl  2  8 > ./amba2f8unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  2  9 > ./amba2f9.v
# perl ./ahb_spec_generator_new_1.5f_vlog.pl  2 10 > ./amba2f10.v
# perl ./ahb_spec_generator_new_1.5f_vlog.pl  2 11 > ./amba2f11.v
# perl ./ahb_spec_generator_new_1.5f_vlog.pl  2 12 > ./amba2f12.v
# perl ./ahb_spec_generator_new_1.5f_vlog.pl  2 13 > ./amba2f13.v
# perl ./ahb_spec_generator_new_1.5f_vlog.pl  2 14 > ./amba2f14.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  3  8 > ./amba3f8unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  3  9 > ./amba3f9.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  4 24 > ./amba4f24unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  4 25 > ./amba4f25.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  5 16 > ./amba5f16unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  5 17 > ./amba5f17.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  6 20 > ./amba6f20unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  6 21 > ./amba6f21.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  7 24 > ./amba7f24unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  7 25 > ./amba7f25.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  8 56 > ./amba8f56unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  8 57 > ./amba8f57.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl  9 32 > ./amba9f32unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl  9 33 > ./amba9f33.v

perl ./ahb_spec_generator_new_1.5f_vlog.pl 10 36 > ./amba10f36unreal.v
perl ./ahb_spec_generator_new_1.5f_vlog.pl 10 37 > ./amba10f37.v

#----------------------------------------------------------------------------
# Creating benchmarks using genbuf_spec_generator_1.1c_vlog.pl:
#----------------------------------------------------------------------------
# - it resets the counter (counting how often all fairness assumptions
#   have already been fulfilled) whenever there is SOME progress on the
#   fairness guarantees (and not just when all fairness guarantees have been
#   satisfied in a row)
# - it uses a counter to track fairness property achievements instead
#   of one bit per fairness property
#----------------------------------------------------------------------------
perl ./genbuf_spec_generator_1.1c_vlog.pl  1  2 > ./genbuf1c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  1  3 > ./genbuf1c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  2  2 > ./genbuf2c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  2  3 > ./genbuf2c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  3  2 > ./genbuf3c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  3  3 > ./genbuf3c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  4  2 > ./genbuf4c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  4  3 > ./genbuf4c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  5  2 > ./genbuf5c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  5  3 > ./genbuf5c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  6  2 > ./genbuf6c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  6  3 > ./genbuf6c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  7  2 > ./genbuf7c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  7  3 > ./genbuf7c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  8  2 > ./genbuf8c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  8  3 > ./genbuf8c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  9  2 > ./genbuf9c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  9  3 > ./genbuf9c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  10  2 > ./genbuf10c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  10  3 > ./genbuf10c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  11  2 > ./genbuf11c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  11  3 > ./genbuf11c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  12  2 > ./genbuf12c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  12  3 > ./genbuf12c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  13  2 > ./genbuf13c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  13  3 > ./genbuf13c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  14  2 > ./genbuf14c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  14  3 > ./genbuf14c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  15  2 > ./genbuf15c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  15  3 > ./genbuf15c3.v

perl ./genbuf_spec_generator_1.1c_vlog.pl  16  2 > ./genbuf16c2unreal.v
perl ./genbuf_spec_generator_1.1c_vlog.pl  16  3 > ./genbuf16c3.v

#----------------------------------------------------------------------------
# Creating benchmarks using genbuf_spec_generator_1.1b_vlog.pl:
#----------------------------------------------------------------------------
# - it resets the counter (counting how often all fairness assumptions
#   have already been fulfilled) whenever there is SOME progress on the
#   fairness guarantees (and not just when all fairness guarantees have been
#   satisfied in a row)
# - it uses one bit per fairness property to track if the fairness
#   property has already been visited (instead of a counter)
#----------------------------------------------------------------------------
perl ./genbuf_spec_generator_1.1b_vlog.pl  1  3 > ./genbuf1b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  1  4 > ./genbuf1b4.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  1  5 > ./genbuf1b5.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  1  6 > ./genbuf1b6.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  1  7 > ./genbuf1b7.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  1  8 > ./genbuf1b8.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  1  9 > ./genbuf1b9.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  1 10 > ./genbuf1b10.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  2  3 > ./genbuf2b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  2  4 > ./genbuf2b4.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  2  5 > ./genbuf2b5.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  2  6 > ./genbuf2b6.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  2  7 > ./genbuf2b7.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  2  8 > ./genbuf2b8.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  2  9 > ./genbuf2b9.v
# perl ./genbuf_spec_generator_1.1b_vlog.pl  2 10 > ./genbuf2b10.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  3  3 > ./genbuf3b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  3  4 > ./genbuf3b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  4  3 > ./genbuf4b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  4  4 > ./genbuf4b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  5  3 > ./genbuf5b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  5  4 > ./genbuf5b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  6  3 > ./genbuf6b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  6  4 > ./genbuf6b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  7  3 > ./genbuf7b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  7  4 > ./genbuf7b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  8  3 > ./genbuf8b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  8  4 > ./genbuf8b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  9  3 > ./genbuf9b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  9  4 > ./genbuf9b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  10  3 > ./genbuf10b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  10  4 > ./genbuf10b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  11  3 > ./genbuf11b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  11  4 > ./genbuf11b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  12  3 > ./genbuf12b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  12  4 > ./genbuf12b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  13  3 > ./genbuf13b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  13  4 > ./genbuf13b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  14  3 > ./genbuf14b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  14  4 > ./genbuf14b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  15  3 > ./genbuf15b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  15  4 > ./genbuf15b4.v

perl ./genbuf_spec_generator_1.1b_vlog.pl  16  3 > ./genbuf16b3unreal.v
perl ./genbuf_spec_generator_1.1b_vlog.pl  16  4 > ./genbuf16b4.v

#----------------------------------------------------------------------------
# Creating benchmarks using genbuf_spec_generator_1.1f_vlog.pl:
#----------------------------------------------------------------------------
# - it use a bit per fairness property to track if the fairness
#   property has already been visited (instead of a counter)
# - it resets the counter (counting how often all fairness assumptions
#   have already been fulfilled) only if all fairness guarantees have been
#   satisfied in a row (and not if there is some progress in the guarantees).
#----------------------------------------------------------------------------

perl ./genbuf_spec_generator_1.1f_vlog.pl  1  3 > ./genbuf1f3unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  1  4 > ./genbuf1f4.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  1  5 > ./genbuf1f5.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  1  6 > ./genbuf1f6.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  1  7 > ./genbuf1f7.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  1  8 > ./genbuf1f8.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  1  9 > ./genbuf1f9.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  1 10 > ./genbuf1f10.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  2  3 > ./genbuf2f3unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  2  4 > ./genbuf2f4.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  2  5 > ./genbuf2f5.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  2  6 > ./genbuf2f6.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  2  7 > ./genbuf2f7.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  2  8 > ./genbuf2f8.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  2  9 > ./genbuf2f9.v
# perl ./genbuf_spec_generator_1.1f_vlog.pl  2 10 > ./genbuf2f10.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  3  3 > ./genbuf3f3unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  3  4 > ./genbuf3f4.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  4  3 > ./genbuf4f3unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  4  4 > ./genbuf4f4.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  5  4 > ./genbuf5f4unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  5  5 > ./genbuf5f5.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  6  5 > ./genbuf6f5unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  6  6 > ./genbuf6f6.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  7  6 > ./genbuf7f6unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  7  7 > ./genbuf7f7.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  8  7 > ./genbuf8f7unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  8  8 > ./genbuf8f8.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  9  8 > ./genbuf9f8unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  9  9 > ./genbuf9f9.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  10  9 > ./genbuf10f9unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  10 10 > ./genbuf10f10.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  11 10 > ./genbuf11f10unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  11 11 > ./genbuf11f11.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  12 11 > ./genbuf12f11unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  12 12 > ./genbuf12f12.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  13 12 > ./genbuf13f12unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  13 13 > ./genbuf13f13.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  14 13 > ./genbuf14f13unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  14 14 > ./genbuf14f14.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  15 14 > ./genbuf15f14unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  15 15 > ./genbuf15f15.v

perl ./genbuf_spec_generator_1.1f_vlog.pl  16 15 > ./genbuf16f15unreal.v
perl ./genbuf_spec_generator_1.1f_vlog.pl  16 16 > ./genbuf16f16.v

