#! /usr/bin/perl -w 

##############################################################################
# This script creates Verilog-checkers for the GENBUF [1]
# synthesis benchmarks. The Verilog-checkers are translations of the
# benchmark files for ANZU [2] and RATSY [3] into Verilog. The
# Verilog-checkers have as input all inputs and outputs of the system to be
# synthesized. The Verilog-checkers have only one Boolean output, which
# signals an error. This error output must never go to TRUE for a correct
# implementation of the benchmarks. (This is a simple safety property.)
# The original GENBUF benchmarks contain also fairness (Buechi)
# assumptions and guarantees. They are translated into safety properties as
# follows.
# - We reduce all fairness assumptions into one fairness assumption X using
#   a 'special' counting construction: we introduce one bit of memory per
#   fairness assumption to store if this assumption has already been visited.
# - We reduce all fairness guarantees into one fairness guarantee Y in the
#   same way.
# - We add one more counter Z. It is incremented whenever X visits and accepting
#   state. It is reset whenever there is some progress in Y (at least one new
#   fairness guarantee visited). It must never
#   reach a given value (otherwise the error-output will go to TRUE).
#   This enforces a certain ratio between fairness guarantee achievements and
#   fairness assumption achievements.
# [1] http://www.iaik.tugraz.at/content/research/design_verification/anzu/cocv.pdf
# [2] http://www.iaik.tugraz.at/content/research/design_verification/anzu/
# [3] http://rat.fbk.eu/ratsy/
#
# Usage: ./genbuf_spec_generator_1.1nrb_vlog.pl <num_of_senders> <fairness_ratio>
#
##############################################################################

use strict;
use POSIX; # qw(ceil floor);

my $ip = "i_";             # the prefix for input variables
my $op = "controllable_";  # the prefix for output variables

sub slc {
    my $j = shift;
    my $bits = shift;
    my $assign = "";
    my $val;

    for (my $bit = 0; $bit < $bits; $bit++) {
	$val = $j % 2;
	$assign .= "SLC$bit=$val";
	$assign .= " * " unless ($bit == $bits-1);
	$j = floor($j/2);
    }
    return $assign;
}

sub slcP {
    my $j = shift;
    my $bits = shift;
    my $assign = "";
    my $val;

    for (my $bit = 0; $bit < $bits; $bit++) {
        $val = $j % 2;
        if($val == 0) {
            $assign .= "~reg_${op}SLC$bit";
        } else {
            $assign .= "reg_${op}SLC$bit";
        }
        $assign .= " & " unless ($bit == $bits-1);
        $j = floor($j/2);
    }
    return $assign;
}

sub slcN {
    my $j = shift;
    my $bits = shift;
    my $assign = "";
    my $val;

    for (my $bit = 0; $bit < $bits; $bit++) {
        $val = $j % 2;
        if($val == 0) {
            $assign .= "~${op}SLC$bit";
        } else {
            $assign .= "${op}SLC$bit";
        }
        $assign .= " & " unless ($bit == $bits-1);
        $j = floor($j/2);
    }
    return $assign;
}

###############################################
# MAIN

if(! defined($ARGV[0])) {
    print "Usage: genbuf_spec_generator_1.1nrb_vlog.pl <num_of_senders> <fairness_ratio k> \n";
    print "        num_of_senders   ... number of senders to the buffer\n";
    print "        fairness_ratio k ... worst case ratio between fairness assumption\n";
    print "                             and fairness guarantee achievements:\n";
    print "                             if all fairness assumptions have been satisfied\n";
    print "                             k times, then all at least one new fairness guarantee\n";
    print "                             has to be fulfilled.\n";
    exit;
}

#variables:
my $num_senders = $ARGV[0];
my $ratio = $ARGV[1];
my $slc_bits = ceil((log $num_senders)/(log 2));
$slc_bits = 1 if ($slc_bits == 0);
my $max_env_fair_count_string = sprintf("%b", $ratio);
my $nr_of_env_fair_count_bits = length($max_env_fair_count_string);
my $max_bit = $nr_of_env_fair_count_bits - 1;
my $max_env_fair_count_vstring = "${nr_of_env_fair_count_bits}'b${max_env_fair_count_string}";
my $num_receivers = 2;
my @input_var_arr;
my @reg_var_arr;
my @env_trans_arr;
my @env_fair_arr;
my @sys_trans_arr;
my @sys_fair_arr;

###############################################
# Communication with senders
#
my $comment = "";
for (my $i=0; $i < $num_senders; $i++) {
    #variable definition
    push(@input_var_arr, "${ip}StoB_REQ$i");
    push(@input_var_arr, "${op}BtoS_ACK$i");
    
    ##########################################
    # Guarantee 2
    $comment = "// G((StoB_REQ$i=0 * X(StoB_REQ$i=1)) -> X(BtoS_ACK$i=0));\t#G2\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~((~(~reg_${ip}StoB_REQ$i & ${ip}StoB_REQ$i )) | ~${op}BtoS_ACK$i)");

    
    $comment = "// G(F(StoB_REQ$i=1 <-> BtoS_ACK$i=1));\t#G1+G2\n";
    push(@sys_fair_arr, $comment."assign sys_fair".@sys_fair_arr." = ${ip}StoB_REQ$i ^~ ${op}BtoS_ACK$i");
    
    # Guarantee 3
    $comment = "// G((BtoS_ACK$i=0 * StoB_REQ$i=0) -> X(BtoS_ACK$i=0));\t#G2\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~((~(~reg_${op}BtoS_ACK$i & ~reg_${ip}StoB_REQ$i )) | ~${op}BtoS_ACK$i)");
    
    # Guarantee 4
    $comment = "// G((BtoS_ACK$i=1 * StoB_REQ$i=1) -> X(BtoS_ACK$i=1));\t#G4\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~((~(reg_${op}BtoS_ACK$i & reg_${ip}StoB_REQ$i )) | ${op}BtoS_ACK$i)");
    
    
    # Assumption 1
    $comment = "// G((StoB_REQ$i=1 * BtoS_ACK$i=0) -> X(StoB_REQ$i=1));\t#A1\n";
    push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~((~(reg_${ip}StoB_REQ$i & ~reg_${op}BtoS_ACK$i)) | ${ip}StoB_REQ$i)");

    $comment = "// G(BtoS_ACK$i=1 -> X(StoB_REQ$i=0));\t#A1\n";
    push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~((~(reg_${op}BtoS_ACK$i)) | ~${ip}StoB_REQ$i)");

    
    # Guarantee 5
    for (my $j=$i+1; $j < $num_senders; $j++) {
        $comment = "// G((BtoS_ACK$i=0) + (BtoS_ACK$j=0));\t#G5\n";
        push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~${op}BtoS_ACK$i | ~${op}BtoS_ACK$j)");
    }
}

###############################################
# Communication with receivers
#
if ($num_receivers != 2) {
    print "Note that the DBW for Guarantee 7 works only for two receivers.\n";
    exit;
}
for (my $j=0; $j < $num_receivers; $j++) {
    #variable definition
    push(@input_var_arr, "${ip}RtoB_ACK$j");
    push(@input_var_arr, "${op}BtoR_REQ$j");
    
    
    # Assumption 2
    $comment = "// G(F(BtoR_REQ$j=1 <-> RtoB_ACK$j=1));\t#A2\n";
    push(@env_fair_arr, $comment."assign env_fair".@env_fair_arr." = ${op}BtoR_REQ$j ^~ ${ip}RtoB_ACK$j");
    
    # Assumption 3
    $comment = "// G(BtoR_REQ$j=0 -> X(RtoB_ACK$j=0));\t#A3\n";
    push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~(reg_${op}BtoR_REQ$j | ~${ip}RtoB_ACK$j)");
    
    # Assumption 4
    $comment = "// G((BtoR_REQ$j=1 * RtoB_ACK$j=1) -> X(RtoB_ACK$j=1));\t#A4\n";
    push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~((~(reg_${op}BtoR_REQ$j & reg_${ip}RtoB_ACK$j)) | ${ip}RtoB_ACK$j)");
    

    # Guarantee 6
    $comment = "// G((BtoR_REQ$j=1 * RtoB_ACK$j=0) -> X(BtoR_REQ$j=1));\t#G6\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~((~(reg_${op}BtoR_REQ$j & ~reg_${ip}RtoB_ACK$j)) | ${op}BtoR_REQ$j)");

    # Guarantee 7
    for (my $k=$j+1; $k < $num_receivers; $k++) {
        $comment = "// G((BtoR_REQ$j=0) + (BtoR_REQ$k=0));\t#G7\n";
        push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~${op}BtoR_REQ$j | ~${op}BtoR_REQ$k)");
    }
    
    # Guarantee 6 and 8
    $comment = "// G(RtoB_ACK$j=1 -> X(BtoR_REQ$j=0));\t#G8\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~reg_${ip}RtoB_ACK$j | ~${op}BtoR_REQ$j)");
}

# DBW for guarantee 7

$comment = "// G((BtoR_REQ0=1 * BtoR_REQ1=1) -> FALSE);\t#G7\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = reg_${op}BtoR_REQ0 & reg_${op}BtoR_REQ1");

$comment = "// G((stateG7_1=0 * stateG7_0=1 * BtoR_REQ0=1) -> FALSE);\t#G7\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = reg_nstateG7_1 & reg_stateG7_0 & reg_${op}BtoR_REQ0");

$comment = "// G((stateG7_1=1 * stateG7_0=1 * BtoR_REQ1=1) -> FALSE);\t#G7\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~reg_nstateG7_1 & reg_stateG7_0 & reg_${op}BtoR_REQ1");


###############################################
# Communication with FIFO and multiplexer
#
#variable definition
push(@input_var_arr, "${ip}FULL");
push(@input_var_arr, "${ip}nEMPTY");
push(@input_var_arr, "${op}ENQ");
push(@input_var_arr, "${op}DEQ");

for (my $bit=0; $bit < $slc_bits; $bit++) {
    push(@input_var_arr, "${op}SLC$bit");
}

# Guarantee 9: ENQ and SLC
my $roseBtoS  = "";
my $roseBtoSComment  = "";
my $roseBtoSi = "";
my $roseBtoSiComment = "";
for (my $i=0; $i < $num_senders; $i++) {
    $roseBtoSiComment = "(BtoS_ACK$i=0 * X(BtoS_ACK$i=1))";
    $roseBtoSi = "(~reg_${op}BtoS_ACK$i & ${op}BtoS_ACK$i)";
    $comment = "// G($roseBtoSiComment -> X(ENQ=1));\t#G9\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~($roseBtoSi) | ${op}ENQ)");
    
    $roseBtoSComment   .=   "(BtoS_ACK$i=1 + X(BtoS_ACK$i=0))";
    $roseBtoSComment   .= " * " if ($i < ($num_senders - 1));
    $roseBtoS   .=   "(reg_${op}BtoS_ACK$i | ~${op}BtoS_ACK$i)";
    $roseBtoS   .= " & " if ($i < ($num_senders - 1));
    if ($i == 0) {
        $comment = "// G($roseBtoSiComment  -> X(".slc($i, $slc_bits)."));\t#G9\n";
        push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~($roseBtoSi) | (".slcN($i, $slc_bits)."))");
    } else {
        $comment = "// G($roseBtoSiComment <-> X(".slc($i, $slc_bits)."));\t#G9\n";
        push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(($roseBtoSi) ^~ (".slcN($i, $slc_bits)."))");
    }
}
$comment = "// G(($roseBtoSComment) -> X(ENQ=0));\t#G9\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~($roseBtoS) | ~${op}ENQ)");

# Guarantee 10
my $fellRtoB = "";
my $fellRtoBComment = "";
for (my $j=0; $j < $num_receivers; $j++) {
    $comment = "// G((RtoB_ACK$j=1 * X(RtoB_ACK$j=0)) -> X(DEQ=1));\t#G10\n";
    push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~(reg_${ip}RtoB_ACK$j & ~${ip}RtoB_ACK$j) | ${op}DEQ)");

    $fellRtoBComment   .=   "(RtoB_ACK$j=0 + X(RtoB_ACK$j=1))";
    $fellRtoBComment   .= " * " if ($j < ($num_receivers - 1));
    $fellRtoB   .=   "(~reg_${ip}RtoB_ACK$j | ${ip}RtoB_ACK$j)";
    $fellRtoB   .= " & " if ($j < ($num_receivers - 1));
}
$comment = "// G(($fellRtoBComment) -> X(DEQ=0));\t#G10\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~($fellRtoB) | ~${op}DEQ)");

# Guarantee 11
$comment = "// G((FULL=1 * DEQ=0) -> ENQ=0);\t#G11\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~(~(${ip}FULL & ~${op}DEQ) | ~${op}ENQ)");


$comment = "// G(EMPTY=1 -> DEQ=0);\t#G11\n";
push(@sys_trans_arr, $comment."assign sys_safe_err".@sys_trans_arr." = ~( ${ip}nEMPTY | ~${op}DEQ)");


# Guarantee 12

$comment = "// G(F(stateG12=0));\t#G12\n";
push(@sys_fair_arr, $comment."assign sys_fair".@sys_fair_arr." = ~reg_stateG12");


# Assumption 4
$comment = "// G((ENQ=1 * DEQ=0) -> X(EMPTY=0));\t#A4\n";
push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~(~(reg_${op}ENQ & ~reg_${op}DEQ) | ${ip}nEMPTY)");


$comment = "// G((DEQ=1 * ENQ=0) -> X(FULL=0));\t#A4\n";
push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~(~(reg_${op}DEQ & ~reg_${op}ENQ) | ~${ip}FULL)");


$comment  = "// G((ENQ=1 <-> DEQ=1) -> ((FULL=1 <-> X(FULL=1)) *\n";
$comment .= "//                         (EMPTY=1 <-> X(EMPTY=1))));\t#A4\n";
push(@env_trans_arr, $comment."assign env_safe_err".@env_trans_arr." = ~(~(reg_${op}ENQ ^~ reg_${op}DEQ) | ((reg_${ip}FULL ^~ ${ip}FULL) &  (reg_${ip}nEMPTY ^~ ${ip}nEMPTY)) )");


###############################################
# PRINT 
###############################################

# collecting together the registers we need:
foreach (@input_var_arr) {
    push(@reg_var_arr, "reg_".$_);
}
push(@reg_var_arr, "reg_stateG7_0");
push(@reg_var_arr, "reg_nstateG7_1");
push(@reg_var_arr, "reg_stateG12");
push(@reg_var_arr, "env_safe_err_happened");
my $nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    push(@reg_var_arr, "env_fair".$f."done");
}
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    push(@reg_var_arr, "sys_fair".$f."done");
}


# print module header:
print "module genbuf_".$num_senders."_new_".$ratio."(";
print "\n        o_err,";
print "\n        i_clk,";
foreach (@input_var_arr) {
    print "\n        ".$_;
    if(\$_ != \$input_var_arr[-1]){
        print ",";
    }
}
print ");\n";
print "\n";

# print inputs:
print "input i_clk;\n";
foreach (@input_var_arr) {
    print "input ".$_.";\n";
}
# print outputs:
print "output o_err;\n";
print "\n";

# print registers:
foreach (@reg_var_arr) {
    print "reg ".$_.";\n";
}
print "reg [$max_bit:0] fair_cnt;\n";
print "\n";


# print wires:
my $cc=0;
foreach (@env_trans_arr) {
    print "wire env_safe_err".$cc.";\n";
    $cc++;
}
print "wire env_safe_err;\n";
print "\n";
$cc=0;
foreach (@sys_trans_arr) {
    print "wire sys_safe_err".$cc.";\n";
    $cc++;
}
print "wire sys_safe_err;\n";
print "\n";
$cc=0;
foreach (@env_fair_arr) {
    print "wire env_fair".$cc.";\n";
    $cc++;
}
print "\n";
$cc=0;
foreach (@sys_fair_arr) {
    print "wire sys_fair".$cc.";\n";
    $cc++;
}
print "wire progress_in_sys_fair;\n";
print "wire all_env_fair_fulfilled;\n";
print "wire all_sys_fair_fulfilled;\n";
print "wire fair_err;\n";
print "wire o_err;\n";
print "\n";

# print assigns for to error signals for ENV_TRANSITION:
print "// =============================================================\n";
print "//                        ENV_TRANSITION:\n";
print "// =============================================================\n";
$cc=0;
my $env_safe_err_string = "assign env_safe_err = ";
foreach (@env_trans_arr) {
    print $_.";\n\n";
    $env_safe_err_string .= "env_safe_err".$cc;
    if($cc != @env_trans_arr - 1) {
        $env_safe_err_string .= " |\n                      ";
    }
    $cc++;
}
print "// collecting together the safety error bits:\n";
print $env_safe_err_string.";\n";
print "\n";

# print assigns for to error signals for SYS_TRANSITION:
print "// =============================================================\n";
print "//                        SYS_TRANSITION:\n";
print "// =============================================================\n";
$cc=0;
my $sys_safe_err_string = "assign sys_safe_err = ";
foreach (@sys_trans_arr) {
    print $_.";\n\n";
    $sys_safe_err_string .= "sys_safe_err".$cc;
    if($cc != @sys_trans_arr - 1) {
        $sys_safe_err_string .= " |\n                      ";
    }
    $cc++;
}
print "// collecting together the safety error bits:\n";
print $sys_safe_err_string.";\n";
print "\n";



# print assigns for to error signals for ENV_FAIRNESS:
print "// =============================================================\n";
print "//                          ENV_FAIRNESS:\n";
print "// =============================================================\n";
foreach (@env_fair_arr) {
    print $_.";\n\n";
}
print "assign all_env_fair_fulfilled = ";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    if($f != 0) {
      print " &\n                                ";
    }
    print "(env_fair${f}done | env_fair${f})";
}
print ";\n";

print "\n";

# print assigns for to error signals for SYS_FAIRNESS:
print "// =============================================================\n";
print "//                          SYS_FAIRNESS:\n";
print "// =============================================================\n";
foreach (@sys_fair_arr) {
    print $_.";\n\n";
}
print "assign all_sys_fair_fulfilled = ";
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    if($f != 0) {
      print " &\n                                ";
    }
    print "(sys_fair${f}done | sys_fair${f})";
}
print ";\n";
print "assign fair_err = (fair_cnt >= $max_env_fair_count_vstring);\n\n";

print "assign progress_in_sys_fair = ";
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
    if($f != 0) {
      print " |\n                                ";
    }
    print "(~sys_fair${f}done & sys_fair${f})";
}
print ";\n";

print "// computing the error output bit:\n";
print "assign o_err = ~env_safe_err & ~env_safe_err_happened & (sys_safe_err | fair_err);\n";


print "initial\n begin\n";
foreach (@reg_var_arr) {
    print "  ".$_." = 0;\n";
}
print "  fair_cnt = 0;\n";

print " end\n";
print "\n";


print "\n";
print "always @(posedge i_clk)\n";
print " begin\n";
print "   // We remember if an environment error occurred:\n";
print "   env_safe_err_happened = env_safe_err_happened | env_safe_err;\n";
print "\n";
print "   // Updating the fairness counters: \n";
print "   if(all_sys_fair_fulfilled)\n";
print "    begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "      env_fair${f}done = 0;\n";
}
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "      sys_fair${f}done = 0;\n";
}
print "      fair_cnt = 0;\n";
print "    end\n";
print "   else\n";
print "    begin\n";
print "      if(progress_in_sys_fair)\n";
print "       begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "         env_fair${f}done = 0;\n";
}
print "         fair_cnt = 0;\n";
$nr_of_fair=@sys_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "         sys_fair${f}done = sys_fair${f}done | sys_fair${f};\n";
}
print "       end\n";
print "      else\n";
print "       begin\n";
print "         if(all_env_fair_fulfilled)\n";
print "          begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "            env_fair${f}done = 0;\n";
}
print "            fair_cnt = fair_cnt + 1;\n";
print "          end\n";
print "         else\n";
print "          begin\n";
$nr_of_fair=@env_fair_arr;
foreach (my $f = 0; $f < $nr_of_fair; $f++) {
print "            env_fair${f}done = env_fair${f}done | env_fair${f};\n";
}
print "          end\n";
print "       end\n";
print "    end\n";
print "\n";
print "   // Updating the automata: \n";
print "   // Automaton G7: \n";
print "   if(reg_nstateG7_1 & ~reg_${op}BtoR_REQ0 & reg_${op}BtoR_REQ1)\n";
print "    begin\n";
print "      reg_nstateG7_1 = 1'b0;\n";
print "      reg_stateG7_0 = 1'b0;\n";
print "    end\n";
print "   else if(~reg_nstateG7_1 & reg_${op}BtoR_REQ0 & ~reg_${op}BtoR_REQ1)\n";
print "    begin\n";
print "      reg_nstateG7_1 = 1'b1;\n";
print "      reg_stateG7_0 = 1'b0;\n";
print "    end\n";
print "   else if(reg_nstateG7_1 & ~reg_${op}BtoR_REQ0 & ~reg_${op}BtoR_REQ1)\n";
print "    begin\n";
print "      reg_nstateG7_1 = 1'b1;\n";
print "      reg_stateG7_0 = 1'b1;\n";
print "    end\n";
print "   else if(~reg_nstateG7_1 & ~reg_${op}BtoR_REQ0 & ~reg_${op}BtoR_REQ1)\n";
print "    begin\n";
print "      reg_nstateG7_1 = 1'b0;\n";
print "      reg_stateG7_0 = 1'b1;\n";
print "    end\n";
print "\n";
print "   // Automaton G12: \n";
print "   if(~reg_stateG12 & reg_${ip}nEMPTY & ~reg_${op}DEQ)\n";
print "      reg_stateG12 = 1'b1;\n";
print "   else if(reg_stateG12 & reg_${op}DEQ)\n";
print "      reg_stateG12 = 1'b0;\n";
print "\n";
print "   // Latching the previous input:\n";
foreach (@input_var_arr) {
    if($_ ne "${ip}hburst0" && $_ ne "${ip}hburst1"){
        print "   reg_".$_." =  ".$_.";\n";
    }
}

print "\n";
print " end\n";

print "endmodule\n";

